const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujae9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
/*
function getAccountByUserID(req, res) {
  console.log('GET /apitechu/v2/accounts/:id');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var id = req.params.id;
  console.log("Id del usuario a traer " + id);
  var query = 'q={"email":' + '"' + id + '"' + '}';

  console.log(query);

  httpClient.get("accounts1?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "Sin cuentas"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )

}
*/
function getAccountByUserID(req, res) {
  console.log('GET /apitechu/v2/accounts/:id');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var id = req.params.id;
  console.log("Id del usuario a traer las cuentas " + id);
  var query = 'q={"id":' + '"' + id + '"' + '}';

  console.log(query);

  httpClient.get("accounts1?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "Sin cuentas"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )

}



function getAccountByIBAN(req, res) {
  console.log('GET /apitechu/v2/movimientos');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  console.log(req.body)

  var IBAN = req.params.IBAN;
  console.log("cuenta " + IBAN);
  var query = 'q={"IBAN":"' + IBAN + '"}';
  console.log(query)

  httpClient.get("accounts1?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo cuenta"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "Sin cuenta"
          }
          res.status(404);
        }
      }
      //var cuentas = response.movimientos
      console.log(response);
      res.send(response);
    }
  )

}

function updateBalanceAccount(req, res) {
  console.log('POST /apitechu/v2/accounts/:account')

  var newOperacion = {
    "IBAN": req.body.IBAN,
    "importe": req.body.importe,
    "tipo": req.body.tipo
  }

  console.log(newOperacion.IBAN);
  console.log(newOperacion.importe);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  console.log("hola");
  var query = 'q={"IBAN":' + '"' + newOperacion.IBAN + '"' + '}';
  console.log(query);

  httpClient.get("accounts1?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo cuenta"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body [0];
        } else {
          var response = {
            "msg" : "Sin cuenta"
          }
          res.status(404);
        }
      }
      console.log(response.balance);
      console.log("sumamos " + response.balance + "y " + newOperacion.importe)

      if (newOperacion.tipo == "ingreso") {
        var newBalance = parseInt(response.balance) + parseInt(newOperacion.importe);
      } else {
        var newBalance = parseInt(response.balance) - parseInt(newOperacion.importe);
      }

      var listadoMovimientos = response.movimientos
      //console.log(listadoMovimientos)
      var newMovimiento = {
        "fecha": Date(),
        "tipo" : newOperacion.tipo,
        "importe": newOperacion.importe
      }
      listadoMovimientos.push(newMovimiento);
      console.log(listadoMovimientos)

      var actualizarMovimiento = {"$set":{"balance": newBalance,"movimientos": listadoMovimientos}};

      console.log(actualizarMovimiento);
      httpClient.put("accounts1?" + query + "&" + mLabAPIKey, actualizarMovimiento,
        function(errPUT, resMLabPUT, bodyPUT) {
          console.log("Movimiento guardado con exito");
        }
      )
    }
  )
}




module.exports.getAccountByUserID = getAccountByUserID;
module.exports.getAccountByIBAN = getAccountByIBAN;
module.exports.updateBalanceAccount = updateBalanceAccount;
//module.exports.createAccount = createAccount;
