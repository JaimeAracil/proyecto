const io = require('../io.js');
const crypt = require('../crypt.js');
const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujae9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function sleep(millis)
{
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < millis);
}

function getUsersV1(req, res) {
  console.log('GET /apitechu/v1/users');

  // res.sendFile('usuarios.json', {root: __dirname});
  var users = require('../usuarios.json');

  var result = {};

  var top = req.query.$top;
  var count = req.query.$count;

  if (count){
    result.count = users.length;
  }

  result.users = top ?
    users.slice(0, top) : users;

  res.send(result);
}

function getUsersV2(req, res) {
  console.log('GET /apitechu/v2/users');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMlab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }

      res = Object.keys(response).length;
      return res
      //res.send(response);
    }
  )
}

/*
function getUsersByIdV2(req, res) {
  console.log('GET /apitechu/v2/users/:id');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var id = req.params.id;
  console.log("Id del usuario a traer " + id);
  var query = 'q={"email":' + '"' + id + '"' + '}';

  httpClient.get("user1?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body [0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}
*/

function getUsersByIdV2(req, res) {
  console.log('GET /apitechu/v2/users/:id');

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var id = req.params.id;
  console.log("Id del usuario a traer " + id);
  var query = 'q={"id":' + '"' + id + '"' + '}';

  httpClient.get("user1?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body [0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      console.log("mando el usuario")
      res.send(response);
    }
  )
}


function createUsersV1(req, res) {
    console.log("POST /apitechu/v1/users");

    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.password);

    var newUser = {
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": req.body.password
    }

    var users = require('../usuarios.json');
    users.push(newUser);
    console.log("Usuario añadido");
    io.writeUserDataToFile(users);
}

function createUsersV2(req, res) {
    console.log("POST /apitechu/v2/users");

    var httpClient = requestJson.createClient(baseMlabURL);

    console.log("Client created");

    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.password);

    var id = Math.random().toString(36).slice(2);

    var newUser = {
      "id": id,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": crypt.hash(req.body.password)
    };

    var ktnr, iban;
    var pruef, pruef2;
    ktnr = (Math.round(Math.random() * 8999999) + 1000000);
    pruef = ((ktnr * 1000000) + 43);
    pruef2 = pruef % 97;
    pruef = 98 - pruef2;
    if (pruef > 9)
    {
      iban = "DE";
    }
    else
    {
      iban = "DE0";
    }
    iban = iban + pruef + "70050000" + "000" + ktnr;
    console.log(iban);

    var query = 'q={"email":' + '"' + newUser.email + '"' + '}';

    console.log(query);

    httpClient.get("user1?" + query + "&" + mLabAPIKey,
      function(err, resMlab, body) {

        if (err) {
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var response = {
              "msg" : "El usuario ya existe"
            };
            console.log("el usuario ya existe")
          } else {
            console.log("el usuario no existe")
            httpClient.post("user1?" + mLabAPIKey, newUser,
              function(err, resMLab, body) {
                console.log("Usuario guardado con exito");
                //res.status(201).send({"msg" : "Usuario guardado con exito"})
              }
          )




            var newAccount = {
              "id" : id,
              //"email" : newUser.email,
              "IBAN" : iban,
              "balance" : 0,
              "movimientos" : [{
                "fecha": Date(),
                "tipo": "inicial",
                "importe": 0
              }]
            };

            httpClient.post("accounts1?"+ mLabAPIKey, newAccount,
              function(err, resMLab, body) {
                console.log("Cuenta creada con exito");
                //res.status(201).send({"msg" : "Usuario guardado con exito"})
              }
          )
            res.status(200);
          }
        }
        res.send(response);
      }
    )




}

function deleteUsersV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  var deleted = false;

  // console.log("Usando for normal");
  // for (var i = 0; i < users.length; i++) {
  //   console.log("comparando " + users[i].id + " y " +  req.params.id);
  //   if (users[i].id == req.params.id) {
  //     console.log("La posicion " + i + " coincide");
  //     users.splice(i, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for in");
  // for (arrayId in users) {
  //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
  //   if (users[arrayId].id == req.params.id) {
  //     console.log("La posicion " + arrayId " coincide");
  //     users.splice(arrayId, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of");
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición ? coincide");
  //     // Which one to delete? order is not guaranteed...
  //     deleted = false;
  //     break;
  //   }
  // }

  // console.log("Usando for of 2");
  // // Destructuring, nodeJS v6+
  // for (var [index, user] of users.entries()) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of 3");
  // var index = 0;
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  //   index++;
  // }

  // console.log("Usando Array ForEach");
  // users.forEach(function (user, index) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //   }
  // });

  // console.log("Usando Array findIndex");
  // var indexOfElement = users.findIndex(
  //   function(element){
  //     console.log("comparando " + element.id + " y " +   req.params.id);
  //     return element.id == req.params.id
  //   }
  // )
  //
  // console.log("indexOfElement es " + indexOfElement);
  // if (indexOfElement >= 0) {
  //   users.splice(indexOfElement, 1);
  //   deleted = true;
  // }

  if (deleted) {
    // io.writeUserDataToFile(users);
    io.writeUserDataToFile(users);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUsersV1 = createUsersV1;
module.exports.createUsersV2 = createUsersV2;
module.exports.deleteUsersV1 = deleteUsersV1;
