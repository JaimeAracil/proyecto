const io = require('../io');
const crypt = require('../crypt.js');
const requestJson = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujae9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1(req, res) {
 console.log("POST /apitechu/v1/login");

 var email = req.body.email;
 var password = req.body.password;

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.email == email && user.password == password) {
     console.log("Usuario y contraseña correctos");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Usuario logado : " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };

 res.send(response);
 }

function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var email = req.body.email;
  var password = req.body.password;

  console.log(password);

  var query = 'q={"email":"' + email + '"}';
  console.log(query);

  httpClient.get("user1?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {

      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var user = body [0];

          if (crypt.checkPassword(password, user.password) ) {
            console.log("password ok");
            var loggedUserId = user.id;
            user.logged = true;
            console.log("Logged in user with id " + user.id);

            var putBody = '{"$set":{"logged":true}}';

            httpClient.put("user1?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err, resMLab, body) {
                console.log("Usuario guardado con exito");
              }
            )
          }

        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      //res.send(user);
      //if (user.password == password) {

      console.log(loggedUserId);
      var msg = loggedUserId ?
        "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";

      var response = {
          "mensaje": msg,
          "idUsuario": loggedUserId
      };
      console.log(response);
      res.send(response);
    }
  )
}

function logoutV1(req, res) {
 console.log("POST /apitechu/v1/logout/:id");

 var id = req.params.id;

 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == id && user.logged === true) {
     console.log("Usuario logado, procedemos a cierre");
     delete user.logged
     console.log("Cerrando sesión del usuario:  " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }

 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";

 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };

 res.send(response);
}

function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var httpClient = requestJson.createClient(baseMlabURL);
 console.log("Client created");

 var id = req.body.id;
 console.log(id);

 var query = 'q={"id":"' + id + '"}';

 httpClient.get("user1?" + query + "&" + mLabAPIKey,
   function(err, resMlab, body) {

     if (err) {
       var response = {
         "msg" : "Error obteniendo usuario"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var user = body [0];
       } else {
         var user = {
           "msg" : "Usuario no encontrado"
         }
         res.status(404);
       }
     }
     //res.send(user);
     console.log(user);

     if (user.logged === true) {
       console.log("User found, logging out");

       var putBody = '{"$unset":{"logged":""}}';

       httpClient.put("user1?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("Usuario guardado con exito");
         }
       )
       console.log("Realizando logout del usuario:  " + user.id);
       var loggedoutUserId = user.id;
     }
     var msg = loggedoutUserId ?
       "Logout correcto" : "Logout incorrecto";



     var response = {
         "mensaje": msg,
         "idUsuario": loggedoutUserId
     };

     res.send(response);
   }
 )

}

module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
